#version 330 core

layout(location = 0) in vec3 in_position;
layout(location = 1) in vec3 in_colors;

out vec3 color;
uniform mat4 mvp; // K * c_T_w

void main() {
    // clip coordinate
    gl_Position = mvp * vec4(in_position, 1.0);

    color = in_colors;
}
