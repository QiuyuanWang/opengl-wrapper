//
// Created by Neo on 14/08/2017.
//

#include "args.h"

#include <string>
#include <vector>
#include <cassert>
#include <GL/glew.h>

#ifdef USE_CUDA_GL
#include <driver_types.h>
#include <cuda_runtime_api.h>
#include <helper_cuda.h>
#endif

#include <iostream>
#include <glog/logging.h>

namespace gl {
Args::Args() {
  argn_ = 0;
  vao_ = 0;
  vbos_.clear();
#ifdef USE_CUDA_GL
  cuda_res_.clear();
#endif
}

Args::Args(int argn, bool use_cuda) {
  Init(argn, use_cuda);
}

void Args::Init(int argn, bool use_cuda) {
  argn_ = argn;
  use_cuda_ = use_cuda;

  glGenVertexArrays(1, &vao_);
  glBindVertexArray(vao_);

  vbos_.resize(argn_);
  glGenBuffers(argn_, vbos_.data());

#ifdef USE_CUDA_GL
  if (use_cuda) {
    cuda_res_.resize(argn_);
  }
#endif
}

Args::~Args() {
  if (vao_) {
    glDeleteVertexArrays(1, &vao_);
  }
  if (argn_ > 0) {
    glDeleteBuffers(argn_, vbos_.data());
  }

#ifdef USE_CUDA_GL
    for(auto &res:cuda_res_)
        cudaGraphicsUnregisterResource(res);
  cuda_res_.clear();
#endif
}

void Args::InitBuffer(GLuint i,
                      ArgAttrib arg_attrib,
                      size_t max_size) {
  assert((int)i < argn_);
  /// Now we have only ELEMENT_BUFFER and ELEMENT_ARRAY_BUFFER
  if (arg_attrib.buffer != GL_ELEMENT_ARRAY_BUFFER) {
    glEnableVertexAttribArray(i);
  }

  glBindBuffer(arg_attrib.buffer, vbos_[i]);
  glBufferData(arg_attrib.buffer,
               arg_attrib.size * arg_attrib.count * max_size,
               NULL,
               GL_STATIC_DRAW);

  if (use_cuda_) {
#ifdef USE_CUDA_GL
    checkCudaErrors(cudaGraphicsGLRegisterBuffer(
        &cuda_res_[i], vbos_[i], cudaGraphicsMapFlagsNone));
#else
    std::cout << "CUDA unsupported!\n";
    exit(1);
#endif
  }

  if (arg_attrib.buffer != GL_ELEMENT_ARRAY_BUFFER) {
    glVertexAttribPointer(i, arg_attrib.count, arg_attrib.type,
                          GL_FALSE, 0, NULL);
  }
}

void Args::BindBuffer(GLuint i,
                      ArgAttrib arg_attrib,
                      size_t size,
                      void *data) {
  assert((int)i < argn_);
  /// Now we have only ELEMENT_BUFFER and ELEMENT_ARRAY_BUFFER
  if (arg_attrib.buffer != GL_ELEMENT_ARRAY_BUFFER) {
    glEnableVertexAttribArray(i);
  }

  glBindBuffer(arg_attrib.buffer, vbos_[i]);
  if (use_cuda_) {
#ifdef USE_CUDA_GL
    void *map_ptr;
    size_t map_size;
    checkCudaErrors(cudaGraphicsMapResources(1, &cuda_res_[i]));
    checkCudaErrors(cudaGraphicsResourceGetMappedPointer(
        &map_ptr, &map_size, cuda_res_[i]));
    checkCudaErrors(cudaMemcpy(map_ptr, data,
                               arg_attrib.size * arg_attrib.count * size,
                               cudaMemcpyDeviceToDevice));
    checkCudaErrors(cudaGraphicsUnmapResources(1, &cuda_res_[i], NULL));
#else
    std::cout << "CUDA unsupported!\n";
    exit(1);
#endif
  } else {
    glBufferData(arg_attrib.buffer,
                 arg_attrib.size * arg_attrib.count * size,
                 data,
                 GL_STATIC_DRAW);
  }

  if (arg_attrib.buffer != GL_ELEMENT_ARRAY_BUFFER) {
    glVertexAttribPointer(i, arg_attrib.count, arg_attrib.type,
                          GL_FALSE, 0, NULL);
  }
}
}